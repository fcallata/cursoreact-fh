import { useNavigate } from "react-router-dom"

export const LoginPage = () => {

  const navigate = useNavigate();

  const onlogin = () => {
    navigate('/', {
      replace : true
    })
  }

  return (
    <div className="container mt--5">
      <hr />
      <h1>Login Page</h1>
      <button className="btn btn-primary" onClick={onlogin}>Login</button>
    </div>
  )
}
