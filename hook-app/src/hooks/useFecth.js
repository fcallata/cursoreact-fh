import { useEffect, useState } from 'react'

const localCache = {};

export const useFecth = (url) => {

    const [state, setState] = useState({
        data: null,
        isLoading: true,
        hasError: false,
        err: null,
    });

    useEffect(() => {
        getFetch()
    }, [url]);


    const setLoadingState = () => {
      setState({
        data: null,
        isLoading: true,
        hasError: false,
        err: null,
      });
    }
       
    const getFetch = async() => {
      if(localCache[url]){
        console.log("usando cache");
        
        setState({
          data: localCache[url],
          isLoading: false,
          hasError: false,
          err: null,
        })

        return;
      }

        setLoadingState();

        const resp = await fetch(url);

        await new Promise( resolve => setTimeout(resolve, 1500));

        if(!resp.ok){
          setState({
            data: null,
            isLoading: false,
            hasError: true,
            err: {
              code: resp.status,
              message: resp.statusText
            }
        });
        return;
      }

      const data = await resp.json();

      setState({
        data: data,
        isLoading: false,
        hasError: false,
        err: null,
      })
    
      localCache[url] = data;
   
    }

    return {
      data: state.data,
      isLoading: state.isLoading,
      hasError: state.hasError,
    }
}
