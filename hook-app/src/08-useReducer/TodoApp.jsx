import { TodoList } from './TodoList'
import { TodoAdd } from './TodoAdd'
import { useTodo } from '../hooks/useTodo';


export const TodoApp = () => {

    const { todos, todosCount, pendingTodosCount, handleNewTodo, handleDeleteTodo, onToogleTodo } = useTodo();

  return (
    <>
        <h1>Todoapp: { todosCount }, pendientes: {pendingTodosCount}</h1>
        <hr />

        <div className="row">
            <div className="col-7">
                <TodoList 
                    todos={todos}
                    onDeleteTodo={ handleDeleteTodo }
                    onToogleTodo={ onToogleTodo }/>
            </div>

            <div className='col-5'>
                <h4>Agregar todo</h4>
                <hr />

                <TodoAdd onNewTodo={ handleNewTodo }/>
                
            </div>
       </div>
    </>
  )
}
