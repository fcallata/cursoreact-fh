import { useEffect, useState } from 'react'
import { Message } from './Message';

export const SimpleForm = () => {

    const [formState, setFormState] = useState({
        username: 'Strider',
        email: 'fcallata@mail.com'
    })

    const {username, email} = formState;

    const onInputchange = ({target}) => {
        const { name, value } = target;
        console.log({ name, value });

        setFormState({
            ...formState,
            [name]: value
        })
    }

    useEffect(() => {
      //console.log('use effect..');
    }, []);

    useEffect(() => {
        //console.log('use formState..');
    }, [formState]);

    useEffect(() => {
//  console.log('use enauk..');
    }, [email]);
    

  return (
    <>
        <h1>Formu simple</h1>
        <hr />

        <input
            type="text"
            className='form-control'
            placeholder='Username'
            name='username'
            value={username}
            onChange={onInputchange}/>

<       input
            type="email"
            className='form-control'
            placeholder='ejemplo@gmail.com'
            name='email'
            value={email}
            onChange={onInputchange}/>
        {
            (username === 'strider2') && <Message/>
        }

    </>
  )
}
