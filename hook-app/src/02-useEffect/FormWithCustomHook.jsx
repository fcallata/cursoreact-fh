import { useEffect, useState } from 'react'
//import { Message } from './Message';
import { useForm } from '../hooks/useForm';

export const FormWithCustomHook = () => {

    const {formState, onInputchange, username, email, password, onResetForm} = useForm({
        username: '',
        email: '',
        password: ''
    })

    //const {username, email, password} = formState;

    useEffect(() => {
      //console.log('use effect..');
    }, []);

    useEffect(() => {
        //console.log('use formState..');
    }, [formState]);

    useEffect(() => {
//  console.log('use enauk..');
    }, [email]);
    

  return (
    <>
        <h1>Formu con CustomHook</h1>
        <hr />

        <input
            type="text"
            className='form-control'
            placeholder='Username'
            name='username'
            value={username}
            onChange={onInputchange}/>

<       input
            type="email"
            className='form-control'
            placeholder='ejemplo@gmail.com'
            name='email'
            value={email}
            onChange={onInputchange}/>
        
        <input
            type='password'
            className='form-control'
            placeholder='contrasena'
            name='password'
            value={password}
            onChange={onInputchange}/>
       
        <button onClick={onResetForm}>Reset</button>
    </>
  )
}
