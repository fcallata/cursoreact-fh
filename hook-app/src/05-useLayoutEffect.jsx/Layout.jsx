import { useFecth } from '../hooks/useFecth'
import { useCount } from '../hooks/useCount';
import { LoadingMessage } from '../03-examples/LoadingMessage';
import { PokemonCard } from '../03-examples/PokemonCard';

export const Layout = () => {

  const {counter, decrement, increment} = useCount(1);

  console.log('counter: ' + counter);

  const { data, hasError, isLoading } = useFecth(`https://pokeapi.co/api/v2/pokemon/${counter}`);

  return (
    <>
      <h3>Informacion de pokemo</h3>
      <hr />

      {isLoading
        ? <LoadingMessage/>
        : <PokemonCard 
            id={counter}
            name={data.name}
            sprites={[
              data.sprites.front_default,
              data.sprites.front_shiny,
              data.sprites.back_default,
              data.sprites.back_shiny,
            ]}
          />
        }
      {/* <pre>{data?.name}</pre> */}

      <button onClick={ () => increment()}>increment</button>
      <button onClick={ () => counter > 1 ? decrement() : null}>decrement</button>
    </>

  )
}
