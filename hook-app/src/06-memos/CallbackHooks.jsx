import { useCallback, useState } from 'react'
import { ShowIncrement } from './ShowIncrement';

export const CallbackHooks = () => {

    const [counter, setCounter] = useState(10);

    const increment = useCallback(
      (val) => {
        setCounter( (c) => c + val)
      },
      [],
    )

  return (
    <>
        <h1>usecallback {counter}</h1>
        <hr />

        <ShowIncrement increment={ increment} value={10}/>
    </>
  )
}
