import React, { useState } from 'react'
import { useCount } from '../hooks/useCount'
import { Small } from './Small';

export const Memorize = () => {

    const {counter, increment} = useCount(10);
    const [show, setShow] = useState(true)

  return (
    <>
        <h3>Memorize <Small value={counter}/></h3>
        <hr />

        <button onClick={ () => increment()}>+1</button>

        <button onClick={ () => setShow(!show)}> Show/Hide {JSON.stringify(show)}</button>

    </>
  )
}
