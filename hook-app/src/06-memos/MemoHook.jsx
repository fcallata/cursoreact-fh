import React, { useMemo, useState } from 'react'
import { useCount } from '../hooks/useCount'



const heavyStuff = (iterationNumber = 100) => {
  for (let i = 0; i < iterationNumber; i++) {
    console.log("Ahi vamos!");
  }
  return `${iterationNumber} itracion realizadas`;
}

export const MemoHook = () => {

    const {counter, increment} = useCount(10);
    const [show, setShow] = useState(true)

    const memorizeValue = useMemo(() => heavyStuff(counter), [counter])

  return (
    <>
        <h3>Memorize {counter}</h3>
        <hr />

        <h4>{memorizeValue}</h4>

        <button onClick={ () => increment()}>+1</button>

        <button onClick={ () => setShow(!show)}> Show/Hide {JSON.stringify(show)}</button>

    </>
  )
}
