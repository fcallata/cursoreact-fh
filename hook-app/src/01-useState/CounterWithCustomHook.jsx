import React from 'react'
import { useCount } from '../hooks/useCount'

export const CounterWithCustomHook = () => {

  const {counter, increment, decrement, reset} = useCount();

  return (
    <>
        <h1>Counter: {counter}</h1>
        <hr />

        <button className='btn btn-primary' onClick={ () => increment(2)}>+1</button>
        <button className='btn btn-primary' onClick={reset}>reset</button>
        <button className='btn btn-primary' onClick={() => decrement(2)}>-1</button>
    </>
  )
}
