import React from 'react'
import ReactDOM from 'react-dom/client'
import {BrowserRouter} from 'react-router-dom'
import './index.css'
//import { CounterWithCustomHook } from './01-useState/CounterWithCustomHook.jsx'
//import { SimpleForm } from './02-useEffect/SimpleForm.jsx'
//import { FormWithCustomHook } from './02-useEffect/FormWithCustomHook.jsx'
//import { MultipleCustomHooks } from './03-examples/MultipleCustomHooks.jsx'
//import { FocusScreen } from './04-useRef/FocusScreen.jsx'
//import { Layout } from './05-useLayoutEffect.jsx/Layout.jsx'
//import { Memorize } from './06-memos/Memorize.jsx'
//import { MemoHook  from './06-memos/MemoHook.jsx'
//import { CallbackHooks } from './06-memos/CallbackHooks.jsx'
//import { Padre } from './07-tarea-memo/Padre'
//import '../src/08-useReducer/intro-reduce'
//import { TodoApp } from './08-useReducer/TodoApp'
import { MainApp } from './09-useContext/MainApp'

ReactDOM.createRoot(document.getElementById('root')).render(
  <BrowserRouter>
    <MainApp />
  </BrowserRouter>
  //<React.StrictMode>
  //</React.StrictMode>,
)
