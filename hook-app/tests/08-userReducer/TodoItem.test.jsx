import { fireEvent, render, screen } from "@testing-library/react";
import { TodoItem } from "../../src/08-useReducer/TodoItem";

describe('Prueba en <TodoItem/>', () => {

    const todo = {
        id: 1,
        description: 'Piedra del alma',
        donde: false
    }

    const onDeleteTodoMock = jest.fn();
    const onToggleTodoMock = jest.fn();

    beforeEach( () => jest.clearAllMocks() );


    test('Debe mostar eel todo pendiente de completar', () => {
        
        render( <TodoItem 
            todo={todo}
            onToogleTodo={onToggleTodoMock}
            onDeleteTodo={onDeleteTodoMock}/>
        )

        const liElement = screen.getByRole('listitem');
        //console.log(liElement.innerHTML);

        expect(liElement.className).toBe('list-group-item d-flex justify-content-between');

        const spanEl = screen.getByLabelText('span');
        //console.log(spanEl.className);

        expect(spanEl.className).toContain('align-self-center');
        //expect(spanEl.className).toBe('align-self-center ');

        expect(spanEl.className).not.toContain('text-decoration-line-through');

    })

    test('Debe mostar el todo completado', () => {
        
        todo.done = true;

        render( <TodoItem 
            todo={todo}
            onToogleTodo={onToggleTodoMock}
            onDeleteTodo={onDeleteTodoMock}/>
        )

        const spanEl = screen.getByLabelText('span');
        expect(spanEl.className).toContain('text-decoration-line-through');
    })

    test('span debe de llamar el toogle tdo cuan se hace click', () => {
    
        render( <TodoItem 
            todo={todo}
            onToogleTodo={onToggleTodoMock}
            onDeleteTodo={onDeleteTodoMock}/>
        )

        const spanEl = screen.getByLabelText('span');
        fireEvent.click(spanEl);

        expect( onToggleTodoMock ).toHaveBeenCalledWith( todo.id);
    })

    test('button debe de llamr el delete Todo', () => {
    
        render( <TodoItem 
            todo={todo}
            onToogleTodo={onToggleTodoMock}
            onDeleteTodo={onDeleteTodoMock}/>
        )

        const btnel = screen.getByRole('button');
        fireEvent.click(btnel);
        expect( onDeleteTodoMock ).toHaveBeenCalledWith( todo.id);;
    })
 })