import { render, screen } from "@testing-library/react";
import { MultipleCustomHooks } from "../../src/03-examples/MultipleCustomHooks";
import { useFecth } from "../../src/hooks/useFecth";

jest.mock('../../src/hooks/useFecth');

describe('Prueba en <MultipleCustomHos>', () => {

    test('debe mostrar el componente por defecto', () => { 

        useFecth.mockReturnValue({
            data: null,
            isLoading:true,
            hasError: null
        })

        render(<MultipleCustomHooks/>);

        expect( screen.getByText("Cargando"));
        expect( screen.getByText("Informacion de pokemo"));
        const decButton = screen.getByRole( 'button', { name: 'increment'});
        //expect( decButton.disabled ).toBeTruthy();

        //screen.debug();
    })

    test('Prueba con usefetch', () => { 
        /* useFecth.mockReturnValue({
            data: [{name:'pokerat'}],Las pruebas estan hechas con otro api y no coinden
            isLoading: false,
            hasError: null
        }) */
        render(<MultipleCustomHooks/>);
        screen.debug();
        // podemos esperar que los datos que estan esperando se representen
        //
    })

    test('debe llamar a la fucnion incrementar', () => { 
         /* useFecth.mockReturnValue({
            data: [{name:'pokerat'}],Las pruebas estan hechas con otro api y no coinden
            isLoading: false,
            hasError: null
        }) */
        render(<MultipleCustomHooks/>);
        //const btnincrement = screen.getAllByRole('button', { name: 'increment'});
        //expect(handleClick).toHaveBeenCalled()
    })
 })