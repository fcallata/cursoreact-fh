import { renderHook } from "@testing-library/react";
import { useForm } from "../../src/hooks/useForm"
import { act } from "react-dom/test-utils";

describe('Pruebas en useForm', () => { 

    const initialForm = {
        name: "fabio",
        email: "fabio@mail.com"
    }

    test(' Debe regresar los valore por defecto', () => { 
        const {result} = renderHook( ()=> useForm(initialForm));

        expect(result.current).toEqual({
            name: initialForm.name,
            email: initialForm.email,
            formState: initialForm,
            onInputchange: expect.any( Function),
            onResetForm: expect.any( Function),
        })
    })

    test('Debe de cambiar el nombnre del formulario', () => { 
        const {result} = renderHook( () => useForm(initialForm));
        const { onInputchange } = result.current;

        act( () => {
            onInputchange({ target: {name: 'name', value: 'ismael'}});
        });

        expect(result.current.name).toBe('ismael');
        expect(result.current.formState.name).toBe('ismael');

    })

    test('Debe de resetear el nombnre del formulario', () => { 
        const {result} = renderHook( () => useForm(initialForm));
        const { onInputchange, onResetForm } = result.current;

        act( () => {
            onInputchange({ target: {name: 'name', value: 'ismael'}});
            onResetForm();
        });

        expect(result.current.name).toBe('fabio');
        expect(result.current.formState.name).toBe('fabio');

    })
     
 })