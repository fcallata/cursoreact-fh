import { renderHook } from "@testing-library/react";
import { useCount } from "../../src/hooks/useCount"
import { act } from "react-dom/test-utils";

describe('Pruebas en useCounter', () => { 
    test('Debe retornar los valores poor defecto ', () => { 
        const {result} = renderHook( ()=> useCount());
        const { counter, decrement,increment, reset} = result.current;

        expect( counter ).toBe(10);
        expect( decrement ).toEqual( expect.any( Function));
        expect( increment ).toEqual( expect.any( Function));
        expect( reset ).toEqual( expect.any( Function));
        
     });

    test('Debe gnerar el counter de el valor de 100 ', () => {
        const {result} = renderHook( ()=> useCount(100));
        //const { counter, decrement,increment, reset} = result.current;
        //expect( counter ).toBe(100);
        expect( result.current.counter).toBe(100);
    })

    test('debe de incrementar el contador', () => { 
        const { result } = renderHook( () => useCount(20));
        const {counter, increment } = result.current;
        act(() => {
            increment();
        });
        expect(result.current.counter).toBe(21);
     })

     test('debe de decrementar el contador', () => { 
        const { result } = renderHook( () => useCount(20));
        const {counter, decrement } = result.current;
        act(() => {
            decrement();
        });
        expect(result.current.counter).toBe(19);
     })

     test('debe de hacer el reset', () => { 
        const { result } = renderHook( () => useCount(20));
        const {counter, increment, reset } = result.current;
        act(() => {
            increment();
            reset();
        });
        expect(result.current.counter).toBe(20);
     })
 })