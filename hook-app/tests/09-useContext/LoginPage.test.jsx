import { fireEvent, render, screen } from "@testing-library/react"
import { LoginPage } from "../../src/09-useContext/LoginPage"
import { UserContext } from "../../src/09-useContext/context/UserContext"

describe('Pruebas en <LoginPage/>', () => {

    test('Debe de mostrar el componente sin el usuario', () => {
        render(
            <UserContext.Provider value={{user: null}}>
                <LoginPage/>
            </UserContext.Provider>
        )

        const preTag = screen.getByLabelText('pre');
        expect(preTag.innerHTML).toBe('null');
    });

   test('Debe de llamar el SetUser cuando hace click en el boton', () => {

        const setUserMock = jest.fn();
    
        render(
            <UserContext.Provider value={{user: null, setUser: setUserMock}}>
                <LoginPage/>
            </UserContext.Provider>
        );
        const btn = screen.getByRole('button');
        fireEvent.click(btn);

        expect( setUserMock ).toHaveBeenCalledWith({"email": "fcallata@gmail.com", "name": "fabio Callata", "id": 123});

    });
})