import { render, screen } from "@testing-library/react"
import { HomePage } from "../../src/09-useContext/HomePage"
import { UserContext } from "../../src/09-useContext/context/UserContext"


describe('Prubea en <HomePage/>', () => {

    const user = {
        id: 1,
        name: 'Fabio'
    }

    test('debe de mostar el componente sin el usuario', () => {

        render(
            <UserContext.Provider value={{ user: null }}>
                <HomePage/>
            </UserContext.Provider>
        );
        
        const preTag = screen.getByLabelText('pre');
        //console.log(preTag.innerHTML);
        expect(preTag.innerHTML).toBe('null');
    });

    test('debe de mostar el componente cone el usuario', () => {

        render(
            <UserContext.Provider value={{ user: user }}>
                <HomePage/>
            </UserContext.Provider>
        );
        
        const preTag = screen.getByLabelText('pre');
        expect(preTag.innerHTML).toContain(`${user.id}`);
        expect(preTag.innerHTML).toContain(`${user.name}`);
    });

        
})