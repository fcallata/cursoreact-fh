//import porDefecto from './data/heroes';
import heroes, { owners } from './data/heroes';

const getHeroById = (id) => heroes.find((hero) => hero.id === id);

//console.log(getHeroById(2)); // {id: 2, name: "Spiderman", owner: "Marvel"}

const getHeroByOwner = (owner) => heroes.filter((hero) => hero.owner === owner);

//console.log(getHeroByOwner('DC'));

export default getHeroById;