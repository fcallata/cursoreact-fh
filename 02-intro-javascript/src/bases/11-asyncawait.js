const getImagen = async() => {
    try {
        const apiKey = 'hGBYr8Pj9VJLX4bgSlvmUIgkaMge03ZE';
        const urlBase = 'http://api.giphy.com/v1/gifs/random?api_key=' + apiKey;
        const response = await fetch(urlBase);
        const { data } = await response.json();
        const { url } = data.images.original;
        const img = document.createElement('img');
        img.src = url;
        document.body.append(img);
    } catch (error) {
        console.error(error);
    }
}

getImagen();