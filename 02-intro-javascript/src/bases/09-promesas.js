import getHeroById  from "./08-import-export";

// const promesa = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     const heroe = getHeroById(2);
//     console.log(heroe);
//     resolve( heroe );
//     //reject('No se pudo encontrar el héroe');
//   }, 2000);
// });

// promesa.then((res) => {
//   console.log('heroe: ', res);
// })
// .catch((err) => {
//   console.warn(err);
// }
// );

const getHeroByIdAsync = (id) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const heroe = getHeroById(id);
      if (heroe) {
        resolve(heroe);
      } else {
        reject("No se pudo encontrar el héroe");
      }
    }, 2000);
  });
}

getHeroByIdAsync(1)
  .then( heroe => console.log('Heroe: ', heroe))
  .catch(console.warn);
