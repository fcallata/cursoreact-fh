//desestrucutacion de objetos
//Asignacion desestructurante
const persona = {
    nombre: 'Tony',
    edad: 45,
    clave: 'Ironman'
}

//const { nombre } = persona;
//console.log(nombre);


const useContext = ({ nombre, edad, rango = 'Capitan' }) => {
    //console.log(nombre, edad, rango);

    return {
        nombreClave: clave,
        anios: edad,
        latlng: {
            lat: 14.1232,
            lng: -12.3232
        }
    }
}

const avenger = useContext(persona);

const { nombreClave, anios, latlng: { lat, lng } } = avenger;

console.log(nombreClave, anios, lat, lng);


